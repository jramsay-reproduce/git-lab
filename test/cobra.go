package test

import (
	"bytes"

	"github.com/spf13/cobra"
)

// ExecuteCommand executes the command, passing the args and returns the output
// as a string and error
func ExecuteCommand(c *cobra.Command, args ...string) (string, string, error) {
	outBuf := new(bytes.Buffer)
	errBuf := new(bytes.Buffer)

	c.SetOut(outBuf)
	c.SetErr(errBuf)
	c.SetArgs(args)

	_, err := c.ExecuteC()

	return outBuf.String(), errBuf.String(), err
}
