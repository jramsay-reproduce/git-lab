package log

import (
	"os"

	"github.com/sirupsen/logrus"
)

var logger = logrus.StandardLogger()

func init() {
	// Enfore our own defaults on the logrus stdlogger
	logger.Out = os.Stderr
	logger.Formatter = &logrus.TextFormatter{}
	logger.Level = logrus.DebugLevel
}

func Debugf(format string, args ...interface{}) {
	if !isLogEnabled() {
		return
	}

	logger.Debugf(format, args...)
}

func isLogEnabled() bool {
	return os.Getenv("GIT_LAB_DEBUG") == "1"
}
