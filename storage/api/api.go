package api

import (
	"strings"

	"github.com/xanzy/go-gitlab"
)

type GitLabAPI struct {
	client *gitlab.Client
}

func NewClient(host string, accessToken string) (*GitLabAPI, error) {
	if !strings.HasPrefix(host, "http") {
		host = "https://" + host
	}

	client := gitlab.NewClient(nil, accessToken)
	if err := client.SetBaseURL(host); err != nil {
		return nil, err
	}

	return &GitLabAPI{client}, nil
}
