package api

import (
	"gitlab.com/jramsay/git-lab/gitlab"

	gogl "github.com/xanzy/go-gitlab"
)

var (
	scopeAll         = "all"
	sortAscending    = "asc"
	sortDescending   = "desc"
	orderByCreatedAt = "created_at"
)

// Implements IssueReader interface
func (api *GitLabAPI) GetIssue(p *gitlab.Project, id int) (*gitlab.Issue, error) {
	i, _, err := api.client.Issues.GetIssue(p.ID(), id, nil)
	if err != nil {
		return nil, err
	}

	return &gitlab.Issue{*i}, nil
}

// GetIssuesStream implements IssuesReader interface for API storage
func (api *GitLabAPI) GetIssuesStream(p *gitlab.Project, opts *gitlab.IssuesStreamOpts, issueChan chan *gitlab.Issue) error {
	defer close(issueChan)

	apiOpts := &gogl.ListProjectIssuesOptions{
		Scope:   &scopeAll,
		Sort:    &sortAscending,
		OrderBy: &orderByCreatedAt,
		ListOptions: gogl.ListOptions{
			Page:    1,
			PerPage: cap(issueChan),
		},
	}

	if opts != nil {
		apiOpts.UpdatedAfter = opts.UpdatedAfter
	}

	for {
		is, resp, err := api.client.Issues.ListProjectIssues(p.ID(), apiOpts, nil)
		if err != nil {
			break
		}

		for _, i := range is {
			issue := &gitlab.Issue{*i}
			issueChan <- issue
		}

		if resp.CurrentPage >= resp.NextPage {
			break
		}

		apiOpts.Page = resp.NextPage
	}

	return nil
}

func (api *GitLabAPI) CountIssues(p *gitlab.Project, opts *gitlab.IssuesStreamOpts) int {
	apiOpts := &gogl.ListProjectIssuesOptions{
		Scope:        &scopeAll,
		Sort:         &sortDescending,
		OrderBy:      &orderByCreatedAt,
		UpdatedAfter: opts.UpdatedAfter,
		CreatedAfter: nil,
		ListOptions: gogl.ListOptions{
			Page:    1,
			PerPage: 1,
		},
	}

	is, resp, err := api.client.Issues.ListProjectIssues(p.ID(), apiOpts, nil)
	if err != nil {
		return 0
	}

	if resp.TotalItems > 0 {
		return resp.TotalItems
	}

	// For performance reasons, since GitLab 11.8, if the number of resources is
	// greater than 10,000 X-Total, X-Total-Pages, X-Last headers are not set.
	// Fallback to worst case estimation based on the most recent Issue ID since
	// it is most likely a first fetch situation, not an incremental fetch.
	if len(is) > 0 {
		return is[0].IID
	}

	return 0
}
