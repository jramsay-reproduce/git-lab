package api

import (
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/jramsay/git-lab/gitlab"
)

func TestImplementsIssueReaderInterface(t *testing.T) {
	client := &GitLabAPI{}

	var i interface{} = client
	_, ok := i.(gitlab.IssueReader)
	if ok != true {
		t.Fatal("expected gitlab.IssueReader interface")
	}
}

func TestImplementsIssuesReaderInterface(t *testing.T) {
	client := &GitLabAPI{}

	var i interface{} = client
	_, ok := i.(gitlab.IssuesReader)
	if ok != true {
		t.Fatal("expected gitlab.IssuesReader interface")
	}
}

func TestGetIssuesStream(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		f, err := os.Open("../../testdata/list_issues.json")
		if err != nil {
			t.Errorf("error serving data: %v", err)
		}

		io.Copy(w, f)
	}))
	defer ts.Close()

	p := &gitlab.Project{
		Host:      "example.com",
		Name:      "the-project",
		Namespace: "the-group",
	}

	client, err := NewClient(ts.URL, "secret")
	require.NoError(t, err)

	issueChan := make(chan *gitlab.Issue, 20)
	err = client.GetIssuesStream(p, nil, issueChan)
	require.NoError(t, err)

	count := 0
	for range issueChan {
		count++
	}

	require.Equal(t, 10, count)
}
