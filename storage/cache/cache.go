package cache

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"sync"

	"github.com/dgraph-io/badger"
	"gitlab.com/jramsay/git-lab/config"
)

type DB interface {
	// Entry will have their Value field overriden
	Get(Entry) error
	GetStream(chan Entry) error

	Set(Entry) error
	SetStream(chan Entry) error

	// Only the Key value needs to be set to work, although all could be filled
	Delete(Entry) error
	io.Closer
}

type Entry struct {
	Namespace []byte
	Key       int
	Value     interface{}
}

type Cache struct {
	db       *badger.DB
	err      error
	readOnly bool
	hooks    []Hook
}

// Options control the behavior of the cache
type Options struct {
	ReadOnly bool
}

// Hook is a minimal interface that does not, and can't depend on Entry
type Hook interface {
	PostWrite(namespace []byte, key int, value interface{}) error
	PostDelete(namespace []byte, key int) error

	// PostBatchWrite is indented to allow the hook to run in batches, where
	// the control of when the hook runs remains in the control of the hook.
	PostBatchWrite(namespace []byte, key int, value interface{}) error
	// Hooks can depend on BatchFlush being called when the current batch is
	// done.
	BatchFlush() error

	io.Closer
}

var (
	cacheSingleton *Cache
	once           sync.Once

	separator = []byte("\x00")
)

// NewCache creates
func NewCache(opts *Options) (*Cache, error) {
	if opts == nil {
		opts = &Options{ReadOnly: false}
	}

	once.Do(func() {
		dir := config.DataFilePath()
		badgerDB, err := newBadgerDB(dir, opts.ReadOnly)
		cacheSingleton = &Cache{
			db:  badgerDB,
			err: err,
		}

		if opts != nil {
			cacheSingleton.readOnly = opts.ReadOnly
		}
	})

	if opts != nil && cacheSingleton.readOnly != opts.ReadOnly {
		return cacheSingleton, fmt.Errorf("cache singleton already initialized with ReadOnly `%v`, want `%v`", cacheSingleton.readOnly, opts.ReadOnly)
	}

	return cacheSingleton, cacheSingleton.err
}

func newBadgerDB(dataDir string, readOnly bool) (*badger.DB, error) {
	if err := os.MkdirAll(dataDir, os.ModeDir|os.ModePerm); err != nil {
		return nil, err
	}

	opts := badger.DefaultOptions
	opts.ReadOnly = readOnly
	opts.Logger = nil
	opts.Dir, opts.ValueDir = dataDir, dataDir

	badgerDB, err := badger.Open(opts)
	if err != nil {
		return nil, err
	}

	return badgerDB, nil
}

// Get will retrieve the data from the database, and unmarshal it into the value
// passed in as last argument. Make sure that value is a pointer receiver.
func (cache *Cache) Get(namespace []byte, key int, value interface{}) error {
	var data []byte
	err := cache.db.View(func(txn *badger.Txn) error {
		item, err := txn.Get(joinKeys(namespace, itob(key)))
		if err != nil {
			return err
		}

		data, err = item.ValueCopy(nil)
		return err
	})

	if err != nil {
		return err
	}

	return json.Unmarshal(data, value)
}

// Returns a stream of instances of records,
func (cache *Cache) GetStream(namespace []byte, valueChan chan []byte) error {
	return cache.db.View(func(txn *badger.Txn) error {
		defer close(valueChan)

		opts := badger.DefaultIteratorOptions
		opts.Prefix = namespace

		itr := txn.NewIterator(opts)
		defer itr.Close()

		for itr.Rewind(); itr.Valid(); itr.Next() {
			item := itr.Item()

			v, err := item.ValueCopy(nil)
			if err != nil {
				return err
			}

			valueChan <- v
		}

		return nil
	})
}

// Counts the number of records for the given prefix.
func (cache *Cache) CountKeys(namespace []byte) int {
	count := 0
	err := cache.db.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchValues = false
		opts.Prefix = namespace

		itr := txn.NewIterator(opts)
		defer itr.Close()

		for itr.Rewind(); itr.Valid(); itr.Next() {
			count++
		}

		return nil
	})

	if err != nil {
		return 0
	}

	return count
}

// Set will write the value to the key, scoped to the namespace. Once the write
// is commited, the hooks will run. Hooks are not guarenteed to run, as an
// earlier hook might return an error.
func (cache *Cache) Set(e *Entry) error {
	data, err := json.Marshal(e.Value)
	if err != nil {
		return err
	}

	err = cache.db.Update(func(txn *badger.Txn) error {
		return txn.Set(joinKeys(e.Namespace, itob(e.Key)), data)
	})
	if err != nil {
		return err
	}

	return cache.RunPostWriteHooks(e)
}

// RunPostWriteHooks executes PostWrite for every registered hook
func (cache *Cache) RunPostWriteHooks(e *Entry) error {
	for _, hook := range cache.hooks {
		if err := hook.PostWrite(e.Namespace, e.Key, e.Value); err != nil {
			return err
		}
	}

	return nil
}

// SetStream will write the stream of entries to the key, scoped to the
// namespace. After each write (may not be commited yet), the hooks will run.
func (cache *Cache) SetStream(done chan struct{}, entryChan chan *Entry) error {
	wb := cache.db.NewWriteBatch()
	defer wb.Cancel()
	defer close(done)

	for e := range entryChan {
		key := joinKeys(e.Namespace, itob(e.Key))
		data, err := json.Marshal(e.Value)
		if err != nil {
			// TODO Debug logging
			continue
		}

		if err := wb.Set(key, data, 0); err != nil {
			return err
		}

		if err := cache.RunPostBatchWriteHooks(e); err != nil {
			return err
		}
	}

	if err := wb.Flush(); err != nil {
		return err
	}

	return cache.FlushPostBatchWriteHooks()
}

// RunPostBatchWriteHooks executes PostBatchWrite for every registered hook
func (cache *Cache) RunPostBatchWriteHooks(e *Entry) error {
	for _, hook := range cache.hooks {
		if err := hook.PostBatchWrite(e.Namespace, e.Key, e.Value); err != nil {
			return err
		}
	}

	return nil
}

// FlushPostBatchWriteHooks executes BatchFlush for every registered hook
func (cache *Cache) FlushPostBatchWriteHooks() error {
	for _, hook := range cache.hooks {
		if err := hook.BatchFlush(); err != nil {
			return err
		}
	}

	return nil
}

// Delete removes the key, and calls each deleteHook with the key. Hooks are not
// guarenteed to run, as an earlier hook might error before it ran.
func (cache *Cache) Delete(e *Entry) error {
	err := cache.db.Update(func(txn *badger.Txn) error { return txn.Delete(itob(e.Key)) })
	if err != nil {
		return err
	}

	for _, hook := range cache.hooks {
		if err := hook.PostDelete(e.Namespace, e.Key); err != nil {
			return err
		}
	}

	return nil
}

// Close will close the database, and persist to disk.
func (cache *Cache) Close() error {
	if cache.db != nil {
		if err := cache.db.Close(); err != nil {
			return err
		}
		cache.db = nil
	}

	return nil
}

// Close will call the io.Closer on the private cache. Convenience method to be
// able to call Close from the main function
func Close() error {
	if cacheSingleton != nil {
		return cacheSingleton.Close()
	}

	return nil
}

func joinKeys(ks ...[]byte) []byte {
	key := []byte{}

	for i, k := range ks {
		if i > 0 {
			key = append(key, separator...)
		}

		key = append(key, k...)
	}

	return key
}

// itob returns an 8-byte big endian representation of v. Since iteration in
// Badger happens in byte-wise lexicographical sorting order, this means
// iteration happens in descending order by id/createdAt.
func itob(v int) []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, uint64(v))
	return b
}

// RegisterHook registers the provided hook that will be called during cache
// Set and SetStream operations
func (cache *Cache) RegisterHook(hook Hook) {
	cache.hooks = append(cache.hooks, hook)
}
