package cache

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"testing"

	"github.com/dgraph-io/badger"
	"github.com/stretchr/testify/require"
)

func setupCache(dir string) *Cache {
	os.Setenv("XDG_DATA_HOME", dir)

	opts := badger.DefaultOptions
	opts.Dir, opts.ValueDir = dir, dir

	db, err := badger.Open(opts)
	if err != nil {
		panic(err)
	}

	return &Cache{db: db}
}

type testValue struct {
	Value string `json:"value"`
}

func TestCache(t *testing.T) {
	dir, err := ioutil.TempDir("", t.Name())
	if err != nil {
		t.Fatal(err)
	}

	cache := setupCache(dir)
	defer cache.db.Close()

	var (
		prefix = []byte("story")
		key    = 1
		value  = testValue{Value: "T'was brillig"}
	)

	err = cache.Set(&Entry{Namespace: prefix, Key: key, Value: value})
	if err != nil {
		t.Fatal(err)
	}

	var data testValue
	require.NoError(t, cache.Get(prefix, key, &data))
	require.Equal(t, data, value)
}

func TestCacheStream(t *testing.T) {
	dir, err := ioutil.TempDir("", t.Name())
	require.NoError(t, err)

	cache := setupCache(dir)
	defer Close()

	prefix := []byte("story")
	entries := []*Entry{
		&Entry{Key: 1, Value: testValue{"Beware the Jubjub bird"}},
		&Entry{Key: 2, Value: testValue{"The sun was shining on the sea"}},
	}

	writeChan := make(chan *Entry, 2)
	done := make(chan struct{})
	go cache.SetStream(done, writeChan)
	for _, e := range entries {
		writeChan <- e
	}
	close(writeChan)
	<-done

	readChan := make(chan []byte, 2)
	cache.GetStream(prefix, readChan)

	count := 0
	for v := range readChan {
		var tv testValue
		require.NoError(t, json.Unmarshal(v, &tv))

		require.Equal(t, entries[count].Value, tv)
		count++
	}
}

func TestCountKeys(t *testing.T) {
	dir, err := ioutil.TempDir("", t.Name())
	require.NoError(t, err)

	cache := setupCache(dir)
	defer Close()

	prefix := []byte("story")
	entries := []*Entry{
		&Entry{Namespace: prefix, Key: 1, Value: testValue{"Beware the Jubjub bird"}},
		&Entry{Namespace: prefix, Key: 2, Value: testValue{"The sun was shining on the sea"}},
	}

	writeChan := make(chan *Entry, 2)
	done := make(chan struct{})
	go cache.SetStream(done, writeChan)
	for _, e := range entries {
		writeChan <- e
	}
	close(writeChan)
	<-done

	require.Equal(t, 2, cache.CountKeys(prefix))
}

// func TestReadOnlyCache(t *testing.T) {
// 	dir, err := ioutil.TempDir("", t.Name())
// 	require.NoError(t, err)
//
// 	cache := setupCache(dir)
// 	defer Close()
// }
