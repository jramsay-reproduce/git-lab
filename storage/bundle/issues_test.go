package bundle

import (
	"fmt"

	gogl "github.com/xanzy/go-gitlab"
	"gitlab.com/jramsay/git-lab/gitlab"
)

var mockIssues = []*gitlab.Issue{
	&gitlab.Issue{gogl.Issue{IID: 1, Title: "The jaws that bite, the claws that catch!"}},
	&gitlab.Issue{gogl.Issue{IID: 2, Title: "The frumious Bandersnatch!"}},
}

type MockStorage struct{}

func (mock *MockStorage) GetIssuesStream(p *gitlab.Project, opts *gitlab.IssuesStreamOpts, issuesChan chan *gitlab.Issue) error {

	for _, i := range mockIssues {
		issuesChan <- i
	}

	close(issuesChan)

	return nil
}

func (mock *MockStorage) CountIssues(p *gitlab.Project, opts *gitlab.IssuesStreamOpts) int {
	return 0
}

func (mock *MockStorage) UpdateIssuesStream(done chan struct{}, p *gitlab.Project, issuesChan chan *gitlab.Issue) error {
	defer close(done)

	for i := range issuesChan {
		if isMockIssue(i) == false {
			return fmt.Errorf("unexpected update issue %s", i)
		}
	}

	return nil
}

func isMockIssue(i *gitlab.Issue) bool {
	for _, mockIssue := range mockIssues {
		if mockIssue.IID == i.IID {
			return true
		}
	}
	return false
}
