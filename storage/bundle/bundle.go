package bundle

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"encoding/json"
	"fmt"
	"io"
	"time"

	"gitlab.com/jramsay/git-lab/gitlab"
)

type bundleHeader struct {
	Host      string `json:"host"`
	ProjectID string `json:"project_id"`
}

var bundleInfoFilename = "bundle.json"

// ImportBundle reads the input bundle.tar.gz stream from the Reader
func ImportBundle(p *gitlab.Project, gzStream io.Reader, storage gitlab.IssuesWriter) error {
	tarStream, err := gzip.NewReader(gzStream)
	if err != nil {
		return err
	}
	tarReader := tar.NewReader(tarStream)

	bundleIsValid := false

	for {
		header, err := tarReader.Next()

		if err == io.EOF {
			break
		}

		if err != nil {
			return err
		}

		switch header.Name {
		case bundleInfoFilename:
			header := readHeader(tarReader)
			if header == nil {
				return fmt.Errorf("failed to parse bundle config")
			}

			if header.Host != p.Host {
				return fmt.Errorf("bundle config: expected host %s, got %s", p.Host, header.Host)
			}

			if header.ProjectID != p.ID() {
				return fmt.Errorf("bundle config: expected project %s, got %s", p.ID(), header.ProjectID)
			}

			bundleIsValid = true
		case "issues.ndjson":
			if bundleIsValid == false {
				return fmt.Errorf("bundle config not found, `bundle.json` must be the first file in the bundle")
			}

			if err := importIssuesStream(p, tarReader, storage); err != nil {
				return err
			}
		}
	}

	return nil
}

// ExportBundle writes the tar.gz bundle to the provided Writer
func ExportBundle(p *gitlab.Project, tgzStream io.WriteCloser, storage gitlab.IssuesReader) error {
	gzWriter := gzip.NewWriter(tgzStream)
	tarWriter := tar.NewWriter(gzWriter)
	now := time.Now()

	bundleFuncs := []func(*gitlab.Project, io.Writer, gitlab.IssuesReader) (string, error){
		exportHeader,
		exportIssuesStream,
	}

	for _, f := range bundleFuncs {
		buffer := &bytes.Buffer{}
		header := &tar.Header{
			Mode:       0644,
			ModTime:    now,
			AccessTime: now,
			ChangeTime: now,
		}

		name, err := f(p, buffer, storage)
		if err != nil {
			return err
		}

		header.Name = name
		header.Size = int64(buffer.Len())

		if err := tarWriter.WriteHeader(header); err != nil {
			return err
		}

		if _, err := io.Copy(tarWriter, buffer); err != nil {
			return err
		}
	}

	if err := tarWriter.Close(); err != nil {
		return err
	}

	if err := gzWriter.Close(); err != nil {
		return err
	}

	return nil
}

func readHeader(f io.Reader) *bundleHeader {
	decoder := json.NewDecoder(f)

	header := &bundleHeader{}
	if err := decoder.Decode(header); err != nil {
		return nil
	}

	return header
}

func exportHeader(p *gitlab.Project, writer io.Writer, storage gitlab.IssuesReader) (string, error) {
	header := &bundleHeader{
		Host:      p.Host,
		ProjectID: p.ID(),
	}

	encoder := json.NewEncoder(writer)

	if err := encoder.Encode(header); err != nil {
		return "", err
	}

	return bundleInfoFilename, nil
}
