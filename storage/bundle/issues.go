package bundle

import (
	"encoding/json"
	"io"
	"os"

	"gitlab.com/jramsay/git-lab/gitlab"
	"gitlab.com/jramsay/git-lab/reporter"
)

func importIssuesStream(p *gitlab.Project, issueStream io.Reader, storage gitlab.IssuesWriter) error {
	var (
		issueChan    = make(chan *gitlab.Issue, 100)
		progressChan = make(chan int)
		done         = make(chan struct{})
	)

	// FIXME: progress output
	go reporter.PrintProgress(os.Stdout, "Unpacking issues", 0, progressChan)
	go storage.UpdateIssuesStream(done, p, issueChan)

	decoder := json.NewDecoder(issueStream)

	for {
		issue := &gitlab.Issue{}
		if err := decoder.Decode(issue); err == io.EOF {
			break
		} else if err != nil {
			return err
		}

		issueChan <- issue
		progressChan <- 1
	}

	close(issueChan)
	close(progressChan)
	<-done

	return nil
}

func exportIssuesStream(p *gitlab.Project, writer io.Writer, storage gitlab.IssuesReader) (string, error) {
	encoder := json.NewEncoder(writer)

	issueChan := make(chan *gitlab.Issue, 100)
	go storage.GetIssuesStream(p, nil, issueChan)

	for issue := range issueChan {
		if err := encoder.Encode(issue); err != nil {
			return "", err
		}
	}

	return "issues.ndjson", nil
}
