package config

import (
	"os"
	"os/user"
	"path"
	"path/filepath"

	"github.com/BurntSushi/toml"
)

var (
	Config    config
	xdgCache  = "XDG_CACHE_HOME"
	xdgConfig = "XDG_CONFIG_HOME"
	xdgData   = "XDG_DATA_HOME"
	cachePath = "GITLAB_CACHE_PATH"
)

type config struct {
	Servers []Server `toml:"server"`
}

type Server struct {
	AccessToken string `toml:"personal_access_token"`
	Host        string `toml:"host"`
}

func LoadConfig() error {
	configFile := configFilePath()

	f, err := os.Open(configFile)
	if err != nil {
		if os.IsNotExist(err) {
			// TODO: first run behaviour - create file
			return nil
		}
		return err
	}
	defer f.Close()

	Config = config{}
	_, err = toml.DecodeReader(f, &Config)
	return err
}

func CacheFilePath() string {
	// if XDG_CACHE_HOME is set, depend on that, else fallback on $HOMEDIR/.cache/git-lab
	xdgDir := os.Getenv(xdgCache)
	if xdgDir != "" {
		return path.Join(xdgDir, "git-lab")
	}

	usr, _ := user.Current()
	return path.Join(usr.HomeDir, ".cache", "git-lab")
}

func configFilePath() string {
	// if XDG_CONFIG_HOME is set, depend on that, else fallback on $HOME/.config/git-lab
	baseDir := ""
	if xdgDir, found := os.LookupEnv(xdgConfig); found {
		baseDir = xdgDir
	} else {
		usr, _ := user.Current()
		baseDir = filepath.Join(usr.HomeDir, ".config")
	}

	return filepath.Join(baseDir, "git-lab", "gitlab.toml")
}

func DataFilePath() string {
	// if XDG_DATA_HOME is set, depend on that, else fallback on $HOMEDIR/.local/share/git-lab
	xdgDir := os.Getenv(xdgData)
	if xdgDir != "" {
		return path.Join(xdgDir, "git-lab")
	}

	usr, _ := user.Current()
	return path.Join(usr.HomeDir, ".local", "share", "git-lab")
}

func GetServer(hostname string) *Server {
	for _, s := range Config.Servers {
		if hostname == s.Host {
			return &s
		}
	}

	return nil
}
