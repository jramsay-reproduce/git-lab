package reporter

import (
	"fmt"
	"io"
	"os"
	"time"

	"golang.org/x/crypto/ssh/terminal"
)

// PrintProgress is a basic progress reporter for interactive terminals
func PrintProgress(out io.Writer, message string, total int, countChan chan int) {
	startReportAt := time.Now()
	lastReportAt := startReportAt

	count := 0
	for incr := range countChan {
		count += incr

		if shouldReportProgress(incr, lastReportAt) {
			lastReportAt = time.Now()
			fmt.Fprintf(out, "\r%s", progressString(message, count, total, startReportAt))
		}
	}

	// Final status update
	if count > 0 {
		fmt.Fprintf(out, "\r%s, done.\n", progressString(message, count, count, startReportAt))
		return
	}

	return
}

func progressString(message string, count int, total int, startReportAt time.Time) string {
	// Only print progress when there is progress to report
	if count == 0 {
		return ""
	}

	rate := float64(count) / time.Since(startReportAt).Seconds()
	if total == 0 {
		return fmt.Sprintf("%s: %d, %.2f per second", message, count, rate)
	}

	percent := float64(count) / float64(total) * 100
	return fmt.Sprintf("%s: %.f%% (%d/%d), %.2f per second", message, percent, count, total, rate)
}

func shouldReportProgress(incr int, lastReportAt time.Time) bool {
	// Only report progress
	if incr == 0 {
		return false
	}

	// Minimum reporting frequency
	if time.Since(lastReportAt).Seconds() > 10 {
		return true
	}

	// Do not pollute non-terminal environments with chatty progress
	if isTty() == false {
		return false
	}

	if time.Since(lastReportAt) < 500*time.Millisecond {
		return false
	}

	return true
}

var isTerminal *bool

func isTty() bool {
	if isTerminal != nil {
		return *isTerminal
	}

	tty := terminal.IsTerminal(int(os.Stdout.Fd()))
	isTerminal = &tty

	return *isTerminal
}
