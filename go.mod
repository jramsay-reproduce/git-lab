module gitlab.com/jramsay/git-lab

require (
	code.gitea.io/git v0.0.0-20190311071110-74d7c14dd4a3
	github.com/AndreasBriese/bbloom v0.0.0-20190306092124-e2d15f34fcf9 // indirect
	github.com/BurntSushi/toml v0.3.1
	github.com/RoaringBitmap/roaring v0.4.17 // indirect
	github.com/Smerity/govarint v0.0.0-20150407073650-7265e41f48f1 // indirect
	github.com/Unknwon/com v0.0.0-20190214221849-2d12a219ccaf // indirect
	github.com/alecthomas/kingpin v2.2.6+incompatible
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc // indirect
	github.com/alecthomas/units v0.0.0-20151022065526-2efee857e7cf // indirect
	github.com/blevesearch/bleve v0.7.0
	github.com/blevesearch/blevex v0.0.0-20180227211930-4b158bb555a3 // indirect
	github.com/blevesearch/go-porterstemmer v1.0.2 // indirect
	github.com/blevesearch/segment v0.0.0-20160915185041-762005e7a34f // indirect
	github.com/boltdb/bolt v1.3.1 // indirect
	github.com/couchbase/vellum v0.0.0-20190328134517-462e86d8716b // indirect
	github.com/cznic/b v0.0.0-20181122101859-a26611c4d92d // indirect
	github.com/cznic/mathutil v0.0.0-20181122101859-297441e03548 // indirect
	github.com/cznic/strutil v0.0.0-20181122101858-275e90344537 // indirect
	github.com/dgraph-io/badger v2.0.0-rc.2+incompatible
	github.com/dgryski/go-farm v0.0.0-20190323231341-8198c7b169ec // indirect
	github.com/dustin/go-humanize v1.0.0
	github.com/edsrzf/mmap-go v1.0.0 // indirect
	github.com/facebookgo/ensure v0.0.0-20160127193407-b4ab57deab51 // indirect
	github.com/facebookgo/stack v0.0.0-20160209184415-751773369052 // indirect
	github.com/facebookgo/subset v0.0.0-20150612182917-8dac2c3c4870 // indirect
	github.com/fatih/color v1.7.0
	github.com/google/go-cmp v0.2.0
	github.com/jmhodges/levigo v1.0.0 // indirect
	github.com/lithammer/fuzzysearch v1.0.2
	github.com/mattn/go-colorable v0.0.9 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/mcuadros/go-version v0.0.0-20190308113854-92cdf37c5b75 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20190321074620-2f0d2b0e0001 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.5
	github.com/steveyen/gtreap v0.0.0-20150807155958-0abe01ef9be2 // indirect
	github.com/stretchr/testify v1.3.0
	github.com/syndtr/goleveldb v1.0.0 // indirect
	github.com/tecbot/gorocksdb v0.0.0-20181010114359-8752a9433481 // indirect
	github.com/xanzy/go-gitlab v0.16.1
	golang.org/x/crypto v0.0.0-20181203042331-505ab145d0a9
	golang.org/x/net v0.0.0-20190310074541-c10a0554eabf // indirect
	golang.org/x/oauth2 v0.0.0-20190111185915-36a7019397c4 // indirect
	gopkg.in/src-d/go-git.v4 v4.10.0 // indirect
)
