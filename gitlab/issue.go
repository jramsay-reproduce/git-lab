package gitlab

import (
	"time"

	"github.com/xanzy/go-gitlab"
)

const IssueType = "issue"

type Issue struct {
	gitlab.Issue
}

type IssuesStreamOpts struct {
	UpdatedAfter *time.Time
}

func (i *Issue) Type() string {
	return IssueType
}

type IssueReader interface {
	GetIssue(p *Project, id int) (*Issue, error)
}

type IssuesReader interface {
	GetIssuesStream(p *Project, opts *IssuesStreamOpts, readChan chan *Issue) error
	CountIssues(p *Project, opts *IssuesStreamOpts) int
}

type IssueWriter interface {
	UpdateIssue(p *Project, i *Issue) error
}

type IssuesWriter interface {
	UpdateIssuesStream(done chan struct{}, p *Project, writeWrite chan *Issue) error
}

type IssuesReaderWriter interface {
	IssuesWriter
	IssuesReader
}

func StreamIssues(project *Project, opts *IssuesStreamOpts, r IssuesReader, w IssuesWriter, progress chan int) (int, error) {
	var (
		readChan  = make(chan *Issue, 100)
		writeChan = make(chan *Issue, cap(readChan))
		done      = make(chan struct{})
	)

	go r.GetIssuesStream(project, opts, readChan)
	go w.UpdateIssuesStream(done, project, writeChan)

	count := 0
	for i := range readChan {
		writeChan <- i
		if progress != nil {
			progress <- 1
		}
		count++
	}

	// Close write channel and wait for writes to succeed
	close(writeChan)
	if progress != nil {
		close(progress)
	}
	<-done

	return count, nil
}
