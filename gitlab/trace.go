package gitlab

import (
	"fmt"
	"io"
)

type TraceReader struct {
	id         int
	project    *Project
	cursor     int
	err        error
	retries    int
	isFinished bool
	file       io.Reader
}

func NewTraceReader(p *Project, ID int) *TraceReader {
	return &TraceReader{
		id:         ID,
		project:    p,
		cursor:     0,
		err:        nil,
		isFinished: false,
	}
}

func (t *TraceReader) Read(buffer []byte) (int, error) {
	if t.err != nil {
		return 0, t.err
	}

	if t.file == nil {
		// if job is finished, we can return EOF error
		isFinished, err := JobIsFinished(t.project, t.id)
		if err != nil {
			t.err = err
			return 0, t.err
		}
		t.isFinished = isFinished

		f, _, err := t.project.client.Jobs.GetTraceFile(t.project.ID(), t.id, nil)
		if err != nil {
			fmt.Println(err)
			t.err = err
			return 0, t.err
		}
		t.file = f

		offset, err := seek(f, int64(t.cursor))
		if err != nil {
			t.err = err
			return 0, t.err
		}

		// FIXME: don't wait too long for unstarted job? Just return an error
		if offset == 0 {
			t.retries += 1
		}
		if t.retries > 5 {
			t.err = io.EOF
			return 0, t.err
		}
	}

	bytesRead, err := t.file.Read(buffer)
	t.cursor += bytesRead

	// Catch EOF so we can keep fetching the trace
	if err == io.EOF && t.isFinished != true {
		return bytesRead, nil
	}

	return bytesRead, err
}

func (t *TraceReader) Error() string {
	if t.err != nil {
		return t.err.Error()
	}

	return ""
}

// seek sets the offset for the next Read or Write on reader to offset
func seek(r io.Reader, offset int64) (ret int64, err error) {
	buffer := make([]byte, 1)
	cursor := int64(0)

	for {
		// check if we've scanned to where we last finished reading
		if cursor == offset {
			break
		}

		bytesRead, err := r.Read(buffer)
		cursor += int64(bytesRead)

		// catch EOF since nothing new since last poll
		if err == io.EOF && cursor == offset {
			return 0, nil
		} else if err != nil {
			return 0, err
		}
	}

	return cursor, nil
}
