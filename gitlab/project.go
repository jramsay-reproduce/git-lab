package gitlab

import (
	"errors"
	"fmt"
	"net/url"
	"regexp"
	"strings"

	client "github.com/xanzy/go-gitlab"
)

type Project struct {
	Name        string
	Namespace   string
	Host        string
	AccessToken string
	client      *client.Client
}

var (
	patternScpUrl = regexp.MustCompile(`^(?:(?P<user>.+)(?:@))(?P<host>.+?):(?P<project>.+)\.git$`)

	ErrServerNotConfigured = errors.New("No configuration for this server")
	ErrParseProjectPath    = errors.New("Unable to parse project path")
)

// Returns a string containing a project id that can be used for API requests.
func (p *Project) ID() string {
	return fmt.Sprintf("%s/%s", p.Namespace, p.Name)
}

func (p *Project) FullPath() string {
	return fmt.Sprintf("%s/%s/%s", p.Host, p.Namespace, p.Name)
}

func NewProjectFromContext() (*Project, error) {
	repo := NewRepository()

	return repo.GetProject()
}

func NewProjectForRepository(repo *Repository) (*Project, error) {
	return repo.GetProject()
}

// Returns a new Project from the supplied host and project id. The project id
// must be a human readable string (gitlab-org/gitaly) to match the web url and
// clone url.
// WARNING: client will not be set! A refactor is coming!!!
func NewProjectFromOptions(host string, projectID string) (*Project, error) {
	s := strings.Split(projectID, "/")
	namespace, projectName := s[0], s[1]

	return &Project{
		Name:        projectName,
		Namespace:   namespace,
		Host:        host,
		AccessToken: accessTokenForHost(host),
		client:      nil, //-- client is not set because the intention is to remove client entirely from the project
	}, nil

}

func NewProjectFromRemote(remoteURL string) (*Project, error) {
	if patternScpUrl.MatchString(remoteURL) {
		matchGroups := patternScpUrl.FindAllStringSubmatch(remoteURL, -1)

		// Based on the capture groups in the patternScpURL
		u := &url.URL{Host: matchGroups[0][2], Path: "/" + matchGroups[0][3]}
		return NewProjectFromURL(u)
	}

	u, err := url.Parse(remoteURL)
	if err != nil {
		return nil, err
	}
	return NewProjectFromURL(u)
}

func NewProjectFromURL(url *url.URL) (*Project, error) {
	parts := strings.Split(url.Path, "/")

	if len(parts) <= 2 {
		return nil, ErrParseProjectPath
	}

	client, err := NewClient(url.Host)
	if err != nil {
		return nil, err
	}

	name := strings.TrimSuffix(parts[len(parts)-1], ".git")
	namespace := strings.Join(parts[1:len(parts)-1], "/")

	accessToken := accessTokenForHost(url.Host)

	return &Project{name, namespace, url.Host, accessToken, client}, err
}

func FindStringSubmatchMap(r *regexp.Regexp, s string) map[string]string {
	captures := make(map[string]string)

	match := r.FindStringSubmatch(s)
	if match == nil {
		return captures
	}

	for i, name := range r.SubexpNames() {
		// Ignore the whole regexp match and unnamed groups
		if i == 0 || name == "" {
			continue
		}

		captures[name] = match[i]

	}
	return captures
}
