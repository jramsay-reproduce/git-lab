package gitlab

import (
	"strings"
	"testing"
)

func TestNewProjectFromRemote(t *testing.T) {
	inputs := []string{
		"https://gitlab.com/jramsay/git-lab.git",
		"git@gitlab.com:jramsay/git-lab.git",
		"https://gitlab.com/subgroup/jramsay/git-lab.git",
		"git@gitlab.com:subgroup/jramsay/git-lab.git",
	}

	for _, input := range inputs {
		project, err := NewProjectFromRemote(input)
		if err != nil {
			t.Fatalf("error while parsing: %v", err)
		}

		if project.Host != "gitlab.com" {
			t.Fail()
		}

		if project.Name != "git-lab" {
			t.Fail()
		}

		if project.client.BaseURL().Scheme != "https" {
			t.Fail()
		}

		if !strings.HasSuffix(project.Namespace, "jramsay") {
			t.Fatalf("project namespace should end with jramsay, but was %s", project.Namespace)
		}
	}
}
