package gitlab

import (
	"errors"
	"os"
	"strings"

	"code.gitea.io/git"
)

var (
	ErrNoRepository = errors.New("Not a git repository (or any of the parent directories)")
	ErrNoUpstream   = errors.New("No upstream for this repository")
	ErrDetachedHead = errors.New("No branch detected: detached HEAD")
)

// Repository allows for interaction with a Git repository. Retrieving data
// from the repository is done by shelling out to the `git` command. Which
// means `git` must be correctly installed and can be found in $PATH.
type Repository struct {
	Remote string
	Branch string
	HEAD   string

	remoteURL string

	*git.Repository
}

type RepositoryOption func(*Repository)

// NewRepository creates a Repository, without analyzing the context its being
// constructed in.
func NewRepository(opts ...RepositoryOption) *Repository {
	r := &Repository{}

	for _, o := range opts {
		o(r)
	}

	return r
}

// WithRemote sets the remote to use for the requests to the name passed
func WithRemote(remote string) RepositoryOption {
	return func(r *Repository) { r.Remote = remote }
}

// WithHEAD overrides sets the value HEAD would dereference to, for the repository struct to the passed value
// It does not modify the git repository
func WithHEAD(commitID string) RepositoryOption {
	return func(r *Repository) { r.HEAD = commitID }
}

// WithBranch overrides the branch to be used, instead of loading from context
// It does not modify the local git repository
func WithBranch(ref string) RepositoryOption {
	return func(r *Repository) { r.Branch = ref }
}

// GetProject will determine the GitLab project associated based on the current
// checkout. It does so by detecting the upstream tracking branch of the current
// HEAD. ErrNoUpstream will be returned as error, when there's no upstream set.
func (r *Repository) GetProject() (*Project, error) {
	if r.Remote == "" {
		ref, err := RevParseAbbrev("HEAD@{upstream}")
		if err != nil {
			return nil, ErrNoUpstream
		}
		r.Remote = strings.SplitN(ref, "/", 2)[0]
	}

	url, err := UrlForRemote(r.Remote)
	if err != nil {
		return nil, err
	}

	return NewProjectFromRemote(strings.TrimSpace(url))
}

func (r *Repository) ConfigGet(option string) string {
	out, err := git.NewCommand("config", "--get", option).Run()
	if err != nil {
		return ""
	}

	return strings.Trim(strings.TrimSpace(out), "\x00")
}

// Checkout
func (r *Repository) Checkout(ref string) error {
	return git.Checkout(r.Path, git.CheckoutOptions{Branch: ref, Timeout: -1})
}

func (r *Repository) Fetch(ref string) error {
	return git.NewCommand("fetch", r.Remote, ref).RunInDirPipeline(r.Path, os.Stdout, os.Stderr)
}

func (r *Repository) ConfigGetAll(option string) (lines []string) {
	out, err := git.NewCommand("config", "--get-all", option).Run()
	if err != nil {
		return lines
	}

	for _, l := range strings.Split(out, "\n") {
		lines = append(lines, strings.TrimSpace(l))
	}

	return lines
}

func (r *Repository) RefExist(ref string) bool {
	return git.IsReferenceExist(r.Path, ref)
}

func RevParseAbbrev(rev string) (string, error) {
	out, err := git.NewCommand("rev-parse", "--abbrev-ref", rev).Run()
	if err != nil {
		return "", err
	}

	return strings.Trim(strings.TrimSpace(out), "\x00"), nil
}

func OidForRef(revision string) (string, error) {
	out, err := git.NewCommand("rev-parse", "-q", revision).Run()
	if err != nil {
		return "", err
	}

	return strings.SplitN(out, "\n", 2)[0], nil
}

func UrlForRemote(remote string) (string, error) {
	out, err := git.NewCommand("remote", "get-url", remote).Run()
	if err != nil {
		return "", err
	}

	return strings.Trim(strings.SplitN(out, "\n", 2)[0], "\x00"), nil
}

// RemoteForRef returns the short remote name for the local ref provided in the
// format of `refs/heads/feature-branch`.
func RemoteForRef(ref string) (string, error) {
	out, err := git.NewCommand("for-each-ref", "--format='%(upstream:short)'", ref).Run()
	if err != nil {
		return "", err
	}

	remoteRef := strings.Trim(strings.Trim(strings.SplitN(out, "\n", 2)[0], "\x00"), "'")
	remoteName := strings.Split(remoteRef, "/")[0]

	return remoteName, nil
}

// RefForSymbolicRef returns the ref for the provided symbolic ref, typically
// HEAD.
func RefForSymbolicRef(symbol string) (string, error) {
	out, err := git.NewCommand("symbolic-ref", "--quiet", symbol).Run()
	if err != nil {
		return "", err
	}

	return strings.Trim(strings.SplitN(out, "\n", 2)[0], "\x00"), nil
}

// RemoteForHEAD returns the short remote name for the current local branch.
func RemoteForHEAD() (string, error) {
	ref, err := RefForSymbolicRef("HEAD")
	if err != nil {
		return "", nil
	}

	remote, err := RemoteForRef(ref)
	if err != nil {
		return "", nil
	}

	return remote, nil
}

// IsCommit returns true if the provided sha references a commit, and returns
// false for all other input.
func IsCommit(sha string) bool {
	out, err := git.NewCommand("cat-file", "-t", sha).Run()
	if err != nil {
		return false
	}

	if strings.Trim(strings.SplitN(out, "\n", 2)[0], "\x00") == "commit" {
		return true
	}

	return false
}
