package gitlab

import (
	"time"

	g "github.com/xanzy/go-gitlab"
)

type Job struct {
	ID         int
	Name       string
	Stage      string
	Status     string
	FinishedAt *time.Time
}

func FetchPipelineJobsByCommit(p *Project, r *Repository) ([]Job, error) {
	pipelineID, err := p.pipelineIDBySHA(r.HEAD, r.Branch)
	if err != nil {
		return nil, err
	}

	return ListPipelineJobs(p, pipelineID)
}

func JobIsFinished(p *Project, ID int) (bool, error) {
	job, _, err := p.client.Jobs.GetJob(p.ID(), ID, nil)
	if err != nil {
		return false, err
	}

	if job.FinishedAt != nil {
		return true, nil
	}

	return false, nil
}

func ListPipelineJobs(p *Project, ID int) ([]Job, error) {
	var jobs []Job
	opt := &g.ListJobsOptions{
		ListOptions: g.ListOptions{
			Page:    1,
			PerPage: 100,
		},
	}
	for {
		js, resp, err := p.client.Jobs.ListPipelineJobs(p.ID(), ID, opt)
		if err != nil {
			return nil, err
		}

		for _, j := range js {
			jobs = append(jobs, Job{
				ID:         j.ID,
				Name:       j.Name,
				Status:     j.Status,
				Stage:      j.Stage,
				FinishedAt: j.FinishedAt,
			})
		}

		if resp.CurrentPage >= resp.TotalPages {
			break
		}

		opt.Page = resp.NextPage
	}

	return jobs, nil
}

func JobsStatus(jobs []*Job) (s string) {
	count := make(map[string]int, 1)

	for _, j := range jobs {
		if j.Status == "failed" {
			s = j.Status
			return
		}

		count[j.Status]++
	}

	n := len(jobs) - count["manual"]

	switch n {
	case 0:
		return
	case count["pending"]:
		s = "pending"
	case count["cancelled"]:
		s = "cancelled"
	case count["failed"]:
		s = "failed"
	case count["success"]:
		s = "success"
	default:
		s = "running"
	}

	return
}

func deduplicateJobs(jobs []*Job) []*Job {
	distinctJobs := make(map[string]*Job)

	for _, j := range jobs {
		if distinctJobs[j.Name] != nil {
			aFinishedAt := distinctJobs[j.Name].FinishedAt
			bFinishedAt := j.FinishedAt

			if aFinishedAt.After(*bFinishedAt) {
				continue
			}
		}
		distinctJobs[j.Name] = j
	}

	deduped := []*Job{}
	for _, j := range distinctJobs {
		deduped = append(deduped, j)
	}

	return deduped
}

func SummarizeJobsByStage(jobs []*g.Job) map[string]string {
	var mapByStage = map[string][]*g.Job{}

	for _, j := range jobs {
		mapByStage[j.Stage] = append(mapByStage[j.Stage], j)
	}

	var s = make(map[string]string)
	for k, j := range mapByStage {
		s[k] = summarizeJobs(j)
	}

	return s
}

func summarizeJobs(jobs []*g.Job) (s string) {
	var count = map[string]int{}

	s = ""

	for _, j := range jobs {
		if j.Status == "failed" {
			s = j.Status
			return
		}

		count[j.Status]++
	}

	n := len(jobs) - count["manual"]

	switch n {
	case 0:
		return
	case count["pending"]:
		s = "pending"
	case count["cancelled"]:
		s = "cancelled"
	case count["failed"]:
		s = "failed"
	case count["success"]:
		s = "success"
	default:
		s = "running"
	}

	return
}
