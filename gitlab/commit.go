package gitlab

import (
	"time"

	"github.com/xanzy/go-gitlab"
)

type Commit struct {
	gitlab.Commit
	RetrievedAt *time.Time `json:"retrieved_at"`
}

type CommitReader interface {
	GetCommit(*Project, int) (*Commit, error)
}

func GlyphFromStatus(status string) (g string) {
	switch status {
	case "running":
		g = "⠙"
	case "success":
		g = "✓"
	case "failed":
		g = "✗"
	default:
		g = ""
	}
	return
}

func ExitCodeFromStatus(status string) (c int) {
	switch status {
	case "success":
		c = 0
	case "failed":
		c = 1
	case "pending", "running":
		c = 2
	default:
		c = 3
	}
	return
}

func SummarizeCommitStatuses(statuses []*gitlab.CommitStatus) (s string) {
	var count = map[string]int{}

	s = ""

	for _, j := range statuses {
		if j.Status == "failed" {
			s = j.Status
			return
		}

		count[j.Status]++
	}

	n := len(statuses) - count["manual"]

	switch n {
	case 0:
		return
	case count["pending"]:
		s = "pending"
	case count["cancelled"]:
		s = "cancelled"
	case count["failed"]:
		s = "failed"
	case count["success"]:
		s = "success"
	default:
		s = "running"
	}

	return
}
