package issues

import (
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"gitlab.com/jramsay/git-lab/config"
	"gitlab.com/jramsay/git-lab/test"
)

func TestViewIssueCommand_invalid(t *testing.T) {
	out, _, err := test.ExecuteCommand(ViewCommand(), "foobar")
	if err == nil {
		t.Errorf("No errors was defined. Output: %s", out)
	}
}

func TestViewIssueCommand(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		f, err := os.Open("../../../testdata/view_issue.json")
		if err != nil {
			t.Errorf("error serving data: %v", err)
		}

		io.Copy(w, f)
	}))
	defer ts.Close()

	config.LoadConfig()
	config.Config.Servers = []config.Server{config.Server{Host: ts.URL, AccessToken: "secret"}}

	cmd := ViewCommand()

	stdout, stderr, err := test.ExecuteCommand(cmd, "1", "--host", ts.URL, "--project", "gitlab-org/gitaly")
	if err != nil {
		t.Errorf("error viewing issue: %v, stderr: %v", err, stdout)
	}

	if stderr != "" {
		t.Errorf("expected stderr to be emtpy, got: %v", stderr)
	}

	if !strings.HasPrefix(stdout, "# Consider a 'front-end' Chef role") {
		t.Errorf("stdout misformatted: %q", stdout)
	}
}

func TestListIssuesCommand_invalid(t *testing.T) {
	out, _, err := test.ExecuteCommand(ListCommand(), "foobar")
	if err == nil {
		t.Errorf("No errors was defined. Output: %s", out)
	}
}

func TestSearchIssuesCommand_invalid(t *testing.T) {
	out, _, err := test.ExecuteCommand(ListCommand(), "foobar")
	if err == nil {
		t.Errorf("No errors was defined. Output: %s", out)
	}
}
