package issues

import (
	"encoding/json"
	"fmt"
	"io"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/jramsay/git-lab/gitlab"
	"gitlab.com/jramsay/git-lab/internal/index"
	"gitlab.com/jramsay/git-lab/pkg/cli"
	"gitlab.com/jramsay/git-lab/storage/api"
	"gitlab.com/jramsay/git-lab/storage/cache"
)

// Options for viewing, listing and searching issues.
type Options struct {
	IssueID    int
	Project    *gitlab.Project
	ReadCache  bool
	WriteCache bool
	Format     string
	Query      string

	host      string
	projectID string
}

// ViewCommand for viewing issues
func ViewCommand() *cobra.Command {
	opts := Options{}
	eg := `
# prints issue 23 from the current Git repository context
git-lab view issue 23

# prints issue 1 from the project "gitlab-org/gitlab-ce" on the host "gitlab.com"
git-lab view issue 1 --project gitlab-org/gitlab-ce --host gitlab.com
`
	c := &cobra.Command{
		Use:          "issue [id]",
		Short:        "Prints the issue title and description",
		Example:      eg,
		SilenceUsage: true,
		Args:         cli.ExactlyOneIID,
		RunE: func(cmd *cobra.Command, args []string) error {
			opts.IssueID, _ = strconv.Atoi(args[0])

			// If project unspecified, use the current Git repository
			if opts.projectID == "" {
				project, err := gitlab.NewProjectForRepository(gitlab.NewRepository())
				if err != nil {
					return err
				}
				opts.Project = project
			}

			if opts.Project == nil {
				project, err := gitlab.NewProjectFromOptions(opts.host, opts.projectID)
				if err != nil {
					return err
				}
				opts.Project = project
			}

			return viewIssue(cmd.OutOrStdout(), opts)
		},
	}

	cli.AddHostFlag(c, &opts.host)
	cli.AddProjectFlag(c, &opts.projectID)
	cli.AddFormatFlag(c, &opts.Format)

	c.Flags().BoolVar(&opts.ReadCache, "read-cache", false, "read from the offline cache")
	c.Flags().BoolVar(&opts.WriteCache, "write-cache", false, "write to the offline cache")

	return c
}

// ListCommand for listing issues
func ListCommand() *cobra.Command {
	opts := Options{}
	eg := `
# lists issues from the current Git repository context
git-lab list issues

# lists issues from the project "gitlab-org/gitlab-ce" on the host "gitlab.com"
git-lab list issues --project gitlab-org/gitlab-ce --host gitlab.com
`
	c := &cobra.Command{
		Use:          "issues",
		Aliases:      []string{"issue"},
		Short:        "Lists the projects issues",
		Example:      eg,
		SilenceUsage: true,
		Args:         cobra.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {
			// If project unspecified, use the current Git repository
			if opts.projectID == "" {
				project, err := gitlab.NewProjectForRepository(gitlab.NewRepository())
				if err != nil {
					return err
				}
				opts.Project = project
			}

			if opts.Project == nil {
				project, err := gitlab.NewProjectFromOptions(opts.host, opts.projectID)
				if err != nil {
					return err
				}
				opts.Project = project
			}

			return listIssues(cmd.OutOrStdout(), opts)
		},
	}

	cli.AddHostFlag(c, &opts.host)
	cli.AddProjectFlag(c, &opts.projectID)
	cli.AddFormatFlag(c, &opts.Format)

	c.Flags().BoolVarP(&opts.ReadCache, "read-cache", "", false, "read from the offline cache")
	c.Flags().BoolVarP(&opts.WriteCache, "write-cache", "", false, "write to the offline cache")

	return c
}

// SearchCommand for searching issues
func SearchCommand() *cobra.Command {
	opts := Options{}
	eg := `
# searches cached issues for open issues related to the term "concurrency"
git-lab search issues --query "state:opened concurrency"
`
	c := &cobra.Command{
		Use:          "issues",
		Aliases:      []string{"issue"},
		Short:        "Searches the cached issues of a project",
		Example:      eg,
		SilenceUsage: true,
		Args:         cobra.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {
			// If project unspecified, use the current Git repository
			if opts.projectID == "" {
				project, err := gitlab.NewProjectForRepository(gitlab.NewRepository())
				if err != nil {
					return err
				}
				opts.Project = project
			}

			if opts.Project == nil {
				project, err := gitlab.NewProjectFromOptions(opts.host, opts.projectID)
				if err != nil {
					return err
				}
				opts.Project = project
			}

			return searchIssues(cmd.OutOrStdout(), opts)
		},
	}

	cli.AddHostFlag(c, &opts.host)
	cli.AddProjectFlag(c, &opts.projectID)
	cli.AddFormatFlag(c, &opts.Format)

	c.Flags().StringVarP(&opts.Query, "query", "q", "state:opened", "search query")

	return c
}

func viewIssue(out io.Writer, opts Options) error {
	var (
		reader gitlab.IssueReader
		writer gitlab.IssueWriter
	)

	if opts.ReadCache == true || opts.WriteCache == true {
		cacheOpts := &cache.Options{
			ReadOnly: !opts.WriteCache,
		}

		cache, err := cache.NewCache(cacheOpts)
		if err != nil {
			return err
		}

		if opts.ReadCache == true {
			reader = cache
		}

		if opts.WriteCache == true {
			index, err := index.New("issues", &index.Options{ReadOnly: false})
			if err != nil {
				return err
			}
			defer index.Close()

			cache.RegisterHook(index)

			writer = cache
		}
	}

	if opts.ReadCache == false {
		client, err := api.NewClient(opts.Project.Host, opts.Project.AccessToken)
		if err != nil {
			return err
		}

		reader = client
	}

	issue, err := reader.GetIssue(opts.Project, opts.IssueID)
	if err != nil {
		return err
	}

	if writer != nil {
		err = writer.UpdateIssue(opts.Project, issue)
		if err != nil {
			return err
		}
	}

	switch opts.Format {
	case "json":
		data, err := json.Marshal(issue)
		if err != nil {
			return err
		}

		fmt.Fprintln(out, string(data))
	case "text":
		fmt.Fprintf(out, "# %s\n\n%s\n", issue.Title, issue.Description)
	}

	return nil
}

func listIssues(out io.Writer, opts Options) error {
	var (
		reader gitlab.IssuesReader
		writer gitlab.IssuesWriter

		readChan  chan *gitlab.Issue
		writeChan chan *gitlab.Issue
		done      chan struct{}
	)

	if opts.ReadCache == true || opts.WriteCache == true {
		cacheOpts := &cache.Options{
			ReadOnly: !opts.WriteCache,
		}

		cache, err := cache.NewCache(cacheOpts)
		if err != nil {
			return err
		}

		if opts.ReadCache == true {
			reader = cache
		}

		if opts.WriteCache == true {
			index, err := index.New("issues", &index.Options{ReadOnly: false})
			if err != nil {
				return err
			}
			defer index.Close()

			cache.RegisterHook(index)

			writer = cache
		}
	}

	if opts.ReadCache == false {
		client, err := api.NewClient(opts.Project.Host, opts.Project.AccessToken)
		if err != nil {
			return err
		}

		reader = client
	}

	readChan = make(chan *gitlab.Issue, 101)
	go reader.GetIssuesStream(opts.Project, nil, readChan)

	if writer != nil {
		writeChan = make(chan *gitlab.Issue, cap(readChan))
		done = make(chan struct{})

		go writer.UpdateIssuesStream(done, opts.Project, writeChan)
	}

	for issue := range readChan {
		if writer != nil {
			writeChan <- issue
		}

		switch opts.Format {
		case "json":
			data, err := json.Marshal(issue)
			if err != nil {
				return err
			}

			fmt.Fprintln(out, string(data))
		case "text":
			fmt.Fprintf(out, "%d: %s\n", issue.IID, issue.Title)
		}
	}

	if writer != nil {
		// Close write channel and wait for writes to succeed
		close(writeChan)
		<-done
	}

	return nil
}

func searchIssues(out io.Writer, opts Options) error {
	index, err := index.New("issues", &index.Options{ReadOnly: true})
	if err != nil {
		return err
	}
	defer index.Close()

	results, err := index.Search(opts.Query)
	if err != nil {
		return err
	}

	if len(results.Hits) == 0 {
		return fmt.Errorf("no results found for: %s", opts.Query)
	}

	cache, err := cache.NewCache(&cache.Options{ReadOnly: true})
	if err != nil {
		return err
	}

	cache.RegisterHook(index)

	for _, result := range results.Hits {
		id, err := strconv.Atoi(string(result.ID))
		if err != nil {
			return fmt.Errorf("result: %#v, key %q could not be transformed to int: %v", result, result.ID, err)
		}

		i, err := cache.GetIssue(opts.Project, id)
		if err != nil {
			return err
		}

		fmt.Fprintf(out, "%d - %s @ %s\n", i.IID, i.Title, i.WebURL)
	}

	return nil
}
