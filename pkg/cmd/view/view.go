package view

import (
	"github.com/spf13/cobra"
	"gitlab.com/jramsay/git-lab/pkg/cmd/issues"
	"gitlab.com/jramsay/git-lab/pkg/cmd/jobs"
)

// Command returns all related View commands
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "view",
		Short: "View an issue, or job",
		Long:  ``,
	}

	cmd.AddCommand(
		issues.ViewCommand(),
		jobs.ViewCommand(),
	)

	return cmd
}
