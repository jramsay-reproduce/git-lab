package projects

import (
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"gitlab.com/jramsay/git-lab/config"
	"gitlab.com/jramsay/git-lab/test"
)

func TestListProjectsRun_invalid(t *testing.T) {
	out, _, err := test.ExecuteCommand(ListCommand(), "foobar")
	if err == nil {
		t.Errorf("No errors was defined. Output: %s", out)
	}
}

func TestListProjects(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		f, err := os.Open("../../../testdata/list_projects.json")
		if err != nil {
			t.Fatalf("error serving data: %v", err)
		}

		io.Copy(w, f)
	}))
	defer ts.Close()

	config.LoadConfig()
	config.Config.Servers = []config.Server{config.Server{Host: ts.URL, AccessToken: "secret"}}

	cmd := ListCommand()

	stdout, stderr, err := test.ExecuteCommand(cmd, "--starred", "--host", ts.URL)
	if err != nil {
		t.Errorf("error listing projects: %v", stderr)
	}

	if stdout != "Project 4: diaspora/diaspora-client\nProject 6: brightbox/puppet\n" {
		t.Errorf("output misformatted: %q", stdout)
	}
}
