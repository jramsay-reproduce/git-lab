package fetch

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/jramsay/git-lab/gitlab"
	"gitlab.com/jramsay/git-lab/internal/index"
	"gitlab.com/jramsay/git-lab/pkg/cli"
	"gitlab.com/jramsay/git-lab/reporter"
	server "gitlab.com/jramsay/git-lab/storage/api"
	"gitlab.com/jramsay/git-lab/storage/bundle"
	local "gitlab.com/jramsay/git-lab/storage/cache"
)

// Options for caching GitLab projects locally
type Options struct {
	BundlePath string
	BundleURL  string
	Project    *gitlab.Project

	host      string
	projectID string
}

// Command for linting GitLab CI configurations
func Command() *cobra.Command {
	opts := Options{}
	eg := `
# fetches and caches the issues of the gitlab-org/gitlab-ce project
git-lab fetch --project gitlab-org/gitlab-ce --host gitlab.com
`
	c := &cobra.Command{
		Use:          "fetch",
		Short:        "Fetches and caches the issues of the GitLab project",
		Example:      eg,
		SilenceUsage: true,
		Args:         cobra.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {
			if (opts.BundlePath != "") && (opts.BundleURL != "") {
				return errors.New("require --bundle-url or --bundle-path, or neither")
			}

			if opts.projectID != "" {
				project, err := gitlab.NewProjectFromOptions(opts.host, opts.projectID)
				if err != nil {
					return err
				}

				opts.Project = project
			} else {
				project, err := gitlab.NewProjectForRepository(gitlab.NewRepository())
				if err != nil {
					return err
				}

				opts.Project = project
			}

			return fetchIssues(cmd.OutOrStdout(), opts)
		},
	}

	cli.AddHostFlag(c, &opts.host)
	cli.AddProjectFlag(c, &opts.projectID)

	c.Flags().StringVarP(&opts.BundlePath, "bundle-path", "", "", "use local bundle file to prefill the cache")
	c.Flags().StringVarP(&opts.BundleURL, "bundle-url", "", "", "use remote bundle file to prefill the cache")

	return c
}

func fetchIssues(out io.Writer, opts Options) error {
	api, err := server.NewClient(opts.Project.Host, opts.Project.AccessToken)
	if err != nil {
		return err
	}

	cache, err := local.NewCache(nil)
	if err != nil {
		return err
	}
	defer cache.Close()

	if opts.BundlePath != "" {
		fmt.Fprintf(out, "Reading bundle %s\n", opts.BundlePath)
		err := fetchFromBundleFile(opts.Project, cache, opts.BundlePath)
		if err != nil {
			return err
		}
	} else if opts.BundleURL != "" {
		fmt.Fprintf(out, "Reading bundle %s\n", opts.BundleURL)
		err := fetchFromBundleURL(opts.Project, cache, opts.BundleURL)
		if err != nil {
			return err
		}
	}

	// Bypass for full text indexing
	if os.Getenv("GITLAB_CACHE_SKIP_HOOKS") != "true" {
		idx, err := index.New("issues", &index.Options{ReadOnly: false})
		if err != nil {
			return err
		}

		cache.RegisterHook(idx)

		if opts.BundlePath != "" || opts.BundleURL != "" {
			indexProject(out, opts.Project, cache)
		}
	}

	fmt.Fprintln(out, "Fetching updates...")
	if _, err := fetchUpdated(out, opts.Project, api, cache); err != nil {
		return err
	}

	return nil
}

func fetchFromBundleFile(p *gitlab.Project, cache *local.Cache, bundlePath string) error {
	if _, err := os.Stat(bundlePath); err != nil {
		return err
	}

	f, err := os.OpenFile(bundlePath, os.O_RDONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	return bundle.ImportBundle(p, f, cache)
}

func fetchFromBundleURL(p *gitlab.Project, cache *local.Cache, bundleURL string) error {
	resp, err := http.Get(bundleURL)
	if err != nil {
		return err
	}

	return bundle.ImportBundle(p, resp.Body, cache)
}

// Fetch updates since last fetch.
func fetchUpdated(out io.Writer, project *gitlab.Project, r *server.GitLabAPI, w *local.Cache) (int, error) {
	var (
		count int
		err   error

		countChan   = make(chan int)
		lastUpdated = w.GetIssuesUpdatedAt(project)
		now         = time.Now()
		opts        = &gitlab.IssuesStreamOpts{
			UpdatedAfter: lastUpdated,
		}
	)

	go reporter.PrintProgress(out, "Fetching issues", r.CountIssues(project, opts), countChan)

	if count, err = gitlab.StreamIssues(project, opts, r, w, countChan); err != nil {
		return count, err
	}

	err = w.SetIssuesUpdatedAt(project, &now)
	return count, err
}

// indexProject updates the index for every issue in the given project
func indexProject(out io.Writer, p *gitlab.Project, cache *local.Cache) error {
	countChan := make(chan int, 10)
	go reporter.PrintProgress(out, "Indexing issues", cache.CountIssues(p, nil), countChan)

	err := cache.IndexIssues(p, countChan)

	return err
}
