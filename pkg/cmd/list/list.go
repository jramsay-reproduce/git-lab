package list

import (
	"github.com/spf13/cobra"
	"gitlab.com/jramsay/git-lab/pkg/cmd/issues"
	"gitlab.com/jramsay/git-lab/pkg/cmd/jobs"
	"gitlab.com/jramsay/git-lab/pkg/cmd/projects"
)

// Command returns all List commands
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "list",
		Short: "Lists issues, projects, or pipelines",
		Long:  ``,
	}

	cmd.AddCommand(
		issues.ListCommand(),
		jobs.ListCommand(),
		projects.ListCommand(),
	)

	return cmd
}
