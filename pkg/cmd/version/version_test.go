package version

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/jramsay/git-lab/test"
)

func TestVersionCommand(t *testing.T) {
	out, _, err := test.ExecuteCommand(Command(), "")
	require.NoError(t, err)
	require.Contains(t, out, version)
}
