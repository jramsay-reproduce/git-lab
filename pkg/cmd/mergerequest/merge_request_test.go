package mergerequest

import (
	"testing"

	"gitlab.com/jramsay/git-lab/test"
)

func TestCheckoutMergeRequest_invalid(t *testing.T) {
	out, _, err := test.ExecuteCommand(CheckoutCommand(), "foobar")
	if err == nil {
		t.Errorf("No errors was defined. Output: %s", out)
	}
}
