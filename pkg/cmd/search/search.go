package search

import (
	"github.com/spf13/cobra"
	"gitlab.com/jramsay/git-lab/pkg/cmd/issues"
)

// Command returns all Search commands
func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "search",
		Short: "Searches issues",
		Long:  ``,
	}

	cmd.AddCommand(
		issues.SearchCommand(),
	)

	return cmd
}
