package jobs

import (
	"testing"

	"gitlab.com/jramsay/git-lab/test"
)

func TestViewJobCommand_invalid(t *testing.T) {
	out, _, err := test.ExecuteCommand(ViewCommand())
	if err == nil {
		t.Errorf("No errors was defined. Output: %s", out)
	}
}

func TestListJobCommand_invalid(t *testing.T) {
	out, _, err := test.ExecuteCommand(ListCommand(), "foobar")
	if err == nil {
		t.Errorf("No errors was defined. Output: %s", out)
	}
}
