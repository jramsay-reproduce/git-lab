package cli

import (
	"github.com/spf13/cobra"
)

// AddHostFlag adds host flag to the provided command
func AddHostFlag(cmd *cobra.Command, val *string) {
	cmd.Flags().StringVarP(val, "host", "", "gitlab.com", "gitlab host")
}

// AddProjectFlag adds the project flag to the provided command
func AddProjectFlag(cmd *cobra.Command, val *string) {
	cmd.Flags().StringVarP(val, "project", "", "", "project identifier")
}

// AddFormatFlag adds the format flag to the provided command
func AddFormatFlag(cmd *cobra.Command, val *string) {
	cmd.Flags().StringVarP(val, "format", "o", "text", "output format")
}
