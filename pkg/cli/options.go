package cli

import "io"

// StreamOptions define the input and output streams for CLI commands
type StreamOptions struct {
	In  io.Writer
	Out io.Writer
	Err io.Writer
}
