package cli

import (
	"testing"

	"github.com/spf13/cobra"
	"gitlab.com/jramsay/git-lab/test"
)

func emptyRun(cmd *cobra.Command, args []string) {
	return
}

func TestMaximumNArgs(t *testing.T) {
	c := &cobra.Command{Use: "c", Args: ExactlyOneIID, Run: emptyRun}
	output, _, err := test.ExecuteCommand(c, "2")
	if output != "" {
		t.Errorf("Unexpected output: %v", output)
	}
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
}
